<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           Advanced_Post_Filters
 *
 * @wordpress-plugin
 * Plugin Name:       Advanced Post Filters
 * Version:           1.0.0
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Current plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 */

$plugin_path = plugin_dir_path( __FILE__ );

require_once __DIR__ . '/vendor/autoload.php';

define( 'APF\TITLE', 'Advanced Post Filters' );
define( 'APF\NAME', dirname( plugin_basename( $plugin_path ) ) );
define( 'APF\VERSION', '1.0.0' );
define( 'APF\PATH', $plugin_path );
define( 'APF\BASENAME', plugin_basename( $plugin_path ) );
define( 'APF\URL', plugin_dir_url( $plugin_path ) );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-advanced-post-filters-activator.php
 */
function activate_advanced_post_filters() {
	require_once $plugin_path . 'includes/class-activator.php';
	\APF\Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-advanced-post-filters-deactivator.php
 */
function deactivate_advanced_post_filters() {
	require_once $plugin_path . 'includes/class-deactivator.php';
	\APF\Deactivator::deactivate();
}

// register_activation_hook( __FILE__, 'activate_advanced_post_filters' );
// register_deactivation_hook( __FILE__, 'deactivate_advanced_post_filters' );
/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_advanced_post_filters() {

	$plugin = new \APF\Plugin();
	$plugin->run();

}
run_advanced_post_filters();
