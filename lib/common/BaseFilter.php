<?php
/**
 *
 */

namespace APF\Common;

/**
 * Undocumented class
 */
abstract class BaseFilter {

	protected $options;
	protected $fields;

	/**
	 * Setup the Filter
	 *
	 * @param array $options
	 */
	public function __construct( $options = [] ) {

		$this->options = $options;

		$this->title   = $options['title'] ?: '';
		$this->classes = $options['classes'] ?: [ 'filter-set' ];
		$this->id      = $options['id'] ?: '';
		$this->name    = $options['name'] ?: '';

		$this->process();
	}

	/**
	 * Process the options and prepare to filter data or render
	 *
	 * @return void
	 */
	abstract protected function process();

	/**
	 * Filters some data
	 *
	 * @return void
	 */
	abstract protected function filter( \WP_Query $query, $data );

	/**
	 * Renders the Filter form html.
	 *
	 * @return void
	 */
	public function render() {
		printf(
			'<div class="%s">
				<h4>%s</h4>
				<label for="%s">%4$s</label>
				<input id="%3$s" type="text" name="%s">
			</div>',
			implode( ' ', $this->classes ),
			$this->title,
			$this->id,
			$this->name
		);
	}

}

