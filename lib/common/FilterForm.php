<?php
namespace APF\Common;

use APF\Common\SearchFilter;
use APF\Common\TaxFilter;

class FilterForm {

	private $filters;

	public function __construct() {

		$this->addFilter( new SearchFilter() );
		$this->addFilter( new TaxFilter() );

		foreach ( get_declared_classes() as $class_name ) :
			if ( in_array( 'BaseFilter', class_implements( $class_name ) ) ) {
				$this->addFilter( $class_name );
			}
		endforeach;

		// $post_type  = \get_queried_object()->name ?: \get_post_type();
		// $taxonomies = \get_object_taxonomies( $post_type );
		// foreach ( $taxonomies as $tax ) :
		// $tax   = \get_taxonomy( $tax );
		// $terms = \get_terms(
		// [
		// 'taxonomy'   => $tax->name,
		// 'hide_empty' => true,
		// 'fields'     => 'ids',
		// 'parent'     => 0,
		// 'orderby'    => 'count',
		// 'order'      => 'desc',
		// ]
		// );
		// if ( ! $terms ) {
		// continue;
		// }
		// $tax_filter = new CategoryFilter(
		// [
		// 'title'  => $tax->label,
		// 'fields' => array_map(
		// function( $key ) use ( $tax ) {
		// $term = \get_term( $key, $tax->name );
		// return [
		// 'label' => $term->name,
		// 'id'    => "$tax->name-$key",
		// 'count' => $term->count,
		// 'value' => $key,
		// ];
		// }, $terms
		// ),
		// ]
		// );
		// $this->addFilter( $tax_filter );
		// endforeach;
	}

	public function addFilter( BaseFilter $filter ) {
		$this->filters[] = $filter;
	}


	public function filter_posts( $query ) {

		if ( is_admin() || ! $query->is_main_query() ) {
			return;
		}

		foreach ( $this->filters as $filter ) :
			if ( method_exists( $filter, 'filter' ) ) {
				$filter->filter( $query, $_GET );
			}
		endforeach;
	}

	public function render() {

		echo '<form action="" method="GET">';

		foreach ( $this->filters as $filter ) :
			if ( method_exists( $filter, 'render' ) ) {
				$filter->render();
			}
		endforeach;

		echo '<button type="submit">Filter!</button>';
		echo '</form>';
	}
}
