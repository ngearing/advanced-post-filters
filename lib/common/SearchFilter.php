<?php

namespace APF\Common;

class SearchFilter extends BaseFilter {

	public function __construct( $options = [] ) {
		parent::__construct(
			[
				'title' => 'Filter by Search',
				'id'    => 'search-filter',
				'name'  => 'search',
			]
		);
	}

	protected function process() {}

	public function filter( \WP_Query $query, $data ) {

		if ( ! isset( $data[ $this->name ] ) ) {
			return;
		}

		$query->set( 's', $data[ $this->name ] );

	}

	public function render() {
		printf(
			'<div class="%s">
				<h4>%s</h4>
				<label for="%s">Search</label>
				<input id="%3$s" type="text" name="%s" placeholder="Search for..." value="">
			</div>',
			implode( ' ', $this->classes ),
			$this->title,
			$this->id,
			$this->name
		);
	}

}
