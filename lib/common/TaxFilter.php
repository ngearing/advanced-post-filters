<?php

namespace APF\Common;

class TaxFilter extends BaseFilter {

	public function __construct( $options = [] ) {
		parent::__construct(
			[
				'title' => 'Filter by Category',
				'name'  => 'tax',
				'id'    => 'tax-filter',
			]
		);
	}

	protected function process() {

		if ( ! isset( $this->options['fields'] ) ) {
			return;
		}

		$fields = '';

		foreach ( $this->options['fields'] as $field ) {
			$fields .= sprintf(
				'<label for="%s"><input id="%1$s" type="checkbox" name="%s" value="%d"> %s (%s)</label>',
				$field['id'],
				'terms[]',
				$field['value'],
				$field['label'],
				$field['count']
			);
		}

		$this->fields = $fields;
	}

	/**
	 * Filter a post query against data.
	 *
	 * @param object $query
	 * @param array  $data
	 * @return void
	 */
	public function filter( \WP_Query $query, $data ) {
		if ( ! isset( $data[ $this->name ] ) ) {
			return;
		}

		$taxonomies = $data[ $this->name ];
		$terms      = $data['terms'];

		$tax_query = [];
		foreach ( $taxonomies as $tax ) {
			$tax_query[] = [
				'tax'   => $tax,
				'terms' => $terms,
			];
		}

		$query->set( 'tax_query', $tax_query );

	}

	public function render() {
		printf(
			'<div class="%s">
				<h4>%s</h4>
				<fieldset>
					%s
					%s
				</fieldset>
			</div>',
			implode( ' ', $this->classes ),
			$this->title,
			$this->taxes,
			$this->fields
		);
	}

}
