<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Advanced_Post_Filters
 * @subpackage Advanced_Post_Filters/admin
 */

namespace APF;

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Advanced_Post_Filters
 * @subpackage Advanced_Post_Filters/admin
 * @author     Your Name <email@example.com>
 */
class Admin {


	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $Advanced_Post_Filters    The ID of this plugin.
	 */
	private $Advanced_Post_Filters;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string $Advanced_Post_Filters       The name of this plugin.
	 * @param      string $version    The version of this plugin.
	 */
	public function __construct( $Advanced_Post_Filters, $version ) {

		$this->Advanced_Post_Filters = $Advanced_Post_Filters;
		$this->version               = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Advanced_Post_Filters_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Advanced_Post_Filters_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->Advanced_Post_Filters, plugin_dir_url( __FILE__ ) . 'css/advanced-post-filters-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Advanced_Post_Filters_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Advanced_Post_Filters_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->Advanced_Post_Filters, plugin_dir_url( __FILE__ ) . 'js/advanced-post-filters-admin.js', array( 'jquery' ), $this->version, false );

	}

}
