<?php

namespace APF;

class Shortcodes {

	public function __construct( $form ) {
		$this->form = $form;
	}

	public function register_shortcodes() {
		add_shortcode( 'advanced_post_filters', [ $this, 'render_shortcode' ] );
	}

	public function render_shortcode( $atts = [], $content = null ) {
		ob_start();

		$this->form->render();

		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}
}
